FROM node:10-alpine

LABEL maintainer="Satit Rianpit <rianpit@gmail.com>"

WORKDIR /home/api

RUN apk add --upgrade --no-cache --virtual \
  deps \
  python \
  build-base \
  git \
  g++ \
  cairo-dev \
  jpeg-dev \
  pango-dev \
  giflib-dev

COPY . .

RUN npm i

CMD [ "node", "server.js" ]

EXPOSE 3000
