
const fastify = require('fastify')({ logger: true });
const { createCanvas, loadImage, registerFont } = require('canvas');
const numeral = require('numeral');
const moment = require('moment');
const axios = require('axios');

registerFont('fonts/Kanit-Regular.ttf', { family: 'Kanit' })

// Declare a route
fastify.get('/', (req, reply) => {
  reply.redirect('/covid');
})

fastify.get('/covid', async (req, reply) => {
  const canvas = createCanvas(680, 400)
  const ctx = canvas.getContext('2d')

  const toFile = req.query.f;

  try {
    const rs = await axios.get('https://covid19.th-stat.com/api/open/today');
    const data = rs.data;
    const Confirmed = data.Confirmed;
    const Recovered = data.Recovered;
    const Hospitalized = data.Hospitalized
    const Deaths = data.Deaths;
    const NewConfirmed = data.NewConfirmed;
    const NewRecovered = data.NewRecovered;
    const NewHospitalized = data.NewHospitalized;
    const NewDeaths = data.NewDeaths;
    const UpdateDate = data.UpdateDate;

    loadImage('images/demo.jpg').then((image) => {
      ctx.drawImage(image, 0, 0, 680, 400)

      const _confirmed = numeral(Confirmed).format('0,0');
      const _newConfirmed = numeral(NewConfirmed).format('0,0');
      const _newRecovered = numeral(NewRecovered).format('0,0');

      const _updateDate = `${moment(UpdateDate, 'DD/MM/YYYY HH:mm').locale('th').format('D MMM')} ${moment(UpdateDate, 'DD/MM/YYYY HH:mm').get('year') + 543} เวลา ${moment(UpdateDate, 'DD/MM/YYYY HH:mm').format('HH:mm')}`
      ctx.font = '20px Kanit'
      ctx.fillStyle = 'white'
      ctx.fillText(`ข้อมูลวันที่ ${_updateDate}`, 380, 90)

      // data from
      ctx.font = '14px Kanit'
      ctx.fillStyle = 'white'
      ctx.fillText(`ข้อมูลจาก https://covid19.ddc.moph.go.th/th/api`, 370, 110)

      // powered by
      ctx.font = '14px Kanit'
      ctx.fillStyle = 'white'
      ctx.fillText(`Created by Mr.Satit Rianpit`, 480, 390)

      // new confirmed
      ctx.font = '70px Kanit'
      ctx.fillStyle = 'white'
      ctx.fillText(`+${_newConfirmed}`, 50, 280)

      ctx.font = '40px Kanit'
      ctx.fillStyle = 'white'
      ctx.fillText(`คน`, 80, 350)

      // confirmed
      ctx.font = '60px Kanit'
      ctx.fillStyle = 'white'
      ctx.fillText(`${_confirmed}`, 260, 280)

      ctx.font = '40px Kanit'
      ctx.fillStyle = 'white'
      ctx.fillText(`คน`, 300, 350)

      // new recovered
      ctx.font = '60px Kanit'
      ctx.fillStyle = 'white'
      ctx.fillText(`+${_newRecovered}`, 500, 280)

      ctx.font = '40px Kanit'
      ctx.fillStyle = 'white'
      ctx.fillText(`คน`, 540, 350)

      const imageData = canvas.toDataURL();

      if (toFile == 1) {
        const base64Data = imageData.replace(/^data:image\/png;base64,/, '');
        const img = Buffer.from(base64Data, 'base64');

        reply.headers({
          'Content-disposition': 'inline; filename=covid-dashboard.png',
          'Content-type': 'image/png'
        });

        reply.send(img);

      } else {
        reply.headers({ 'Content-Type': 'text/html' });
        const html = `
      <html>
        <header>
          <title>COVID-19 Dashboard canvas</title>
        </header>
        <body style="padding: 0px; margin: 0px;">
          <img style="padding: 0px; margin: 0px;" src="${imageData}"/>
        </body>
      </html>
      `;
        reply.send(html);
      }

    });

  } catch (error) {
    reply.send({ ok: false, error: error.message });
    console.log(error);
  }

})


// Run the server!
fastify.listen(3000, '0.0.0.0', (err) => {
  if (err) {
    fastify.log.error(err)
    process.exit(1)
  }
  fastify.log.info(`server listening on ${fastify.server.address().port}`)
})