# COVID-19 Dashboard Image Generator

สำหรับแสดงภาพสถิติการติดเชื้อของ Covid-19

## Software requirements
- Node.js 10.x
- [Node Canvas](https://github.com/Automattic/node-canvas)
- [Fastify](https://www.fastify.io/)
- ภาพ wallpaper ไปทำเองนะครับ 555++


## Docker

```
docker run -p 88:3000 --name covid-dashboard-canvas -d registry.gitlab.com/siteslave/covid-dashboard-canvas
```

Open [http://localhost:88/covid](http://localhost:88/covid)


Render to file : [http://localhost:88/covid?f=1](http://localhost:88/covid?f=1)

## เอาไปแทรกในหน้าเว็บ

```
 <img src="http://localhost:3000/covid?f=1" />
```

## ตัวอย่าง

![](ss/ss1.png)